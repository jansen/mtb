# mtb

Prussian MTB (Messtischblätter) provide a popular grid which is still important for many biodiversity projects in Germany and neighbouring countries. MTB are map sections from the first Prussian ordnance survey in degree. One MTB is 10 minutes wide and 6 minutes heigh. They can be divided in quarters, quarters of quarters etc.

The division is

|     |      |
| --- | ---- |
| 1 | 2 |
| 3 | 4 |

The following functions will convert MTB or any subdivision of them in number of minutes resp. decimal coordinates or coordinates in MTB-Quadrants. You can choose if you wish coordinates of the center or an edge of the grid.  
Or you can calculate in which MTB (or subdivision) a coordinate (in EPSG 4326) falls.