
###### Debug
mtb2eastnorth('6926', pos='bottomleft', dec=F) == c(600, 2940)
mtb2eastnorth('6827', 'bottomleft', dec=F)     == c(610, 2946)
mtb2eastnorth('69261',  dec=F)                 == c(602.5, 2944.5)
mtb2eastnorth('6926/2',  dec=F)                == c(607.5, 2944.5)
mtb2eastnorth('6926-3',  dec=F)                == c(602.5, 2941.5)
mtb2eastnorth('6926 4',  dec=F)                == c(607.5, 2941.5)
mtb2eastnorth('692611',  dec=F)                == c(601.25, 2945.25)
mtb2eastnorth('692614',  dec=F)                == c(603.75, 2943.75)
mtb2eastnorth('6926', pos='bottomleft', dec=F)  == c(600, 2940)
mtb2eastnorth('69261', pos='bottomleft', dec=F) == c(600, 2943)
mtb2eastnorth('6926/2',pos='bottomleft', dec=F) == c(605, 2943)
mtb2eastnorth('6926-3',pos='bottomleft', dec=F) == c(600, 2940)
mtb2eastnorth('6926 4',pos='bottomleft', dec=F) == c(605, 2940)
mtb2eastnorth('692611',pos='bottomleft', dec=F) == c(600, 2944.5)
mtb2eastnorth('692614',pos='bottomleft', dec=F) == c(602.5, 2943)
mtb2eastnorth('6926', pos='bottomright', dec=F)  == c(610, 2940)
mtb2eastnorth('69261', pos='bottomright', dec=F) == c(605, 2943)
mtb2eastnorth('6926/2',pos='bottomright', dec=F) == c(610, 2943)
mtb2eastnorth('6926-3',pos='bottomright', dec=F) == c(605, 2940)
mtb2eastnorth('6926 4',pos='bottomright', dec=F) == c(610, 2940)
mtb2eastnorth('692611',pos='bottomright', dec=F) == c(602.5, 2944.5)
mtb2eastnorth('692614',pos='bottomright', dec=F) == c(605, 2943)
mtb2eastnorth('6926', pos='topright', dec=F)  == c(610, 2946)
mtb2eastnorth('69261', pos='topright', dec=F) == c(605, 2946)
mtb2eastnorth('6926/2',pos='topright', dec=F) == c(610, 2946)
mtb2eastnorth('6926-3',pos='topright', dec=F) == c(605, 2943)
mtb2eastnorth('6926 4',pos='topright', dec=F) == c(610, 2943)
mtb2eastnorth('692611',pos='topright', dec=F) == c(602.5, 2946)
mtb2eastnorth('692614',pos='topright', dec=F) == c(605, 2944.5)
mtb2eastnorth('6926', pos='topleft', dec=F)  == c(600, 2946)
mtb2eastnorth('69261', pos='topleft', dec=F) == c(600, 2946)
mtb2eastnorth('6926/2',pos='topleft', dec=F) == c(605, 2946)
mtb2eastnorth('6926-3',pos='topleft', dec=F) == c(600, 2943)
mtb2eastnorth('6926 4',pos='topleft', dec=F) == c(605, 2943)
mtb2eastnorth('692611',pos='topleft', dec=F) == c(600, 2946)
mtb2eastnorth('692614',pos='topleft', dec=F) == c(602.5, 2944.5)


mtb2eastnorth(c('6926', '6926/1', '6926134'), pos='center', dec=F) == c(605, 602.5, 601.875, 2943, 2944.5, 2943.375)

mtb2eastnorth(c('6926', '6926/1', '6926134'), pos='center', dec=T) == c(10.08333, 10.04167, 10.03125, 49.05000, 49.07500, 49.05625)


mtb2eastnorth('692612', decimal = T)
# 10.0625 49.0875
#  603.75 2945.25
eastnorth2mtb(mtb2eastnorth('69261234')[1], mtb2eastnorth('69261234')[2])

eastnorth2mtb(mtb2eastnorth('6926 12344221')[1], mtb2eastnorth('6926 12344221')[2], d = 8)

eastnorth2mtb(13.38146,54.08936, d=4)

# You can also construct a MTBQ grid:
# outermost MTBs in Germany: 0901 8756
latlines <- mtb2eastnorth(paste0(sprintf(fmt = "%02d", 9:87), '01', '1'), 'topleft')
lonlines <- mtb2eastnorth(paste0('09', sprintf(fmt = "%02d", 1:56), '4'), 'topleft')

library(sf)
brd <- st_read('/home/jansen/Projekte/GIS/Shapes Germany/germanyWGS84.shp')
plot(brd$geometry)
for(i in ((length(latlines)/2)+1):length(latlines))
  lines(x = c(5.666667,15), y = rep(latlines[i],2))
for(i in 1:(length(lonlines)/2))
  lines(x = rep(lonlines[i],2), y = c(47.3, 55.1))
