############################################# #
## MTB to coordinate
## The Prussian Messtischblatt is still in use for many species observation projects in Germany.
## Die Kartenblätter sind als Gradabteilungskarten aufgebaut, das heißt die Begrenzung der Karten (der Blattschnitt) 
# erfolgt durch ganzzahlige Meridiane und Breitenkreise.
# Jedes Kartenblatt ist 10 Längenminuten breit und 6 Breitenminuten hoch; 
# somit ist das auf einem Blatt abgebildete Gebiet im mittleren Deutschland etwa 10 mal 10 Kilometer groß. 
# Ab 1937 wurde einheitlich eine vierstellige Nummer eingeführt, basierend auf einem Raster. Die ersten zwei Ziffern benennen die Zeile des Rasters, die nächsten zwei die Spalte.
# Ausgangspunkt für die Nummerierung ist 56° Nord und 5° 50’ Ost entsprechend dem damaligen Gebietsstand des Deutschen Reiches. 
# Der nördlichste Punkt lag an der Ostsee beim Ort Nimmersatt im Memelland (Ostpreußen) und findet sich auf dem Blatt 0192.
# Es entspricht beispielsweise der linke untere Eckpunkt des Blattes „6926 Stimpfach“ genau 49° 0’ Nord und 10° 0’ Ost 
# und der rechte obere Eckpunkt 49° 6’ Nord und 10° 10’ Ost. Als Referenzellipsoid wurde für die Karten das Bessel-Ellipsoid benutzt. 
# library(foreign)
# MTB <- read.dbf('~/Projekte/GIS/Shapes Germany/MTB/b25_gk3.dbf', as.is = TRUE)
# mtb.vector <- MTB$DTKNUMMER
# names(mtb.vector) <- MTB$DTKNAME 
# head(mtb.vector)
#     N | E
# MTB 69|26: 10°0'O  49°0'N = 10*60 + 0 = 600'O   49*60 +  0   = 2940'N
# MTB 00|00:                = 600-26*10 = 340'O    2940 + 69*6 = 3354'N
# 19 46 Greifswald
# Positions: "bottomright", "bottomleft", "topleft", "topright", and "center"

mtb2eastnorth <- function(mtb, pos = c('center', "bottomright", "bottomleft", "topleft", "topright"), decimal = TRUE) {
  pos <- match.arg(pos)
  mtb <- as.character(mtb)
  if(any(nchar(mtb) < 4)) stop('MTB grid ID must have at least 4 characters')
  subgrid <- substr(mtb, 5, nchar(mtb))
  subgrid <- gsub('/', '', subgrid, fixed = TRUE)
  subgrid <- gsub('-', '', subgrid, fixed = TRUE)
  subgrid <- gsub(' ', '', subgrid, fixed = TRUE)
  if(any( grepl('[^[1-4]]*', as.numeric(subgrid)))) {
    warning(paste('Found non-valid character (only 1 to 4 allowed) in subgrid(s)', paste(which(grepl('[^[1-4]]*', as.numeric(subgrid))), collapse = ', ')))
    subgrid[grepl('[^[1-4]]*', as.numeric(subgrid))] <- ''
  }
  mtb <- substr(mtb, 1,4)
  N <- substr(mtb, 1,2)
  n <- 3354 - as.numeric(N)*6
  E <- substr(mtb, 3,4)
  e <- 340 + as.numeric(E)*10
  # if(pos %in% c('bottomright','topright'))  e <- e + 10
  # if(pos %in% c('topleft', 'topright'))     n <- n + 6

  ##### adapt for subgrids and non bottomleft positions
  eastwest <- function(s, value, pos) {
    n <- nchar(s)
    if(pos %in% c('bottomright','topright')) value <- value + 10
    if(pos == 'center')                      value <- value + 10/2
    if(pos %in% c('bottomleft','topleft'))   value <- value
    for(i in 0:n) {
        sector <- sapply(s, substr, i, i, USE.NAMES = FALSE)
        if(sector == '')           value <- value
        if(sector %in% c('1','3') & pos %in% c('center'))                     value <- value - 10/2^(i+1)
           if(sector %in% c('1','3') & pos %in% c('bottomright','topright'))  value <- value - 10/2^(i)
        if(sector %in% c('2','4') & pos == 'center')                         value <- value + 10/2^(i+1)
        if(sector %in% c('2','4') & pos %in% c('bottomleft','topleft'))      value <- value + 10/2^(i)
    }
    return(value)
  }
  northsouth <- function(s, value, pos) {
    n <- nchar(s)
    if(pos %in% c('bottomleft', 'bottomright')) value <- value
    if(pos == 'center')                         value <- value + 6/2
    if(pos %in% c('topleft','topright'))        value <- value + 6
    for(i in 0:n) {
      sector <- sapply(s, substr, i, i, USE.NAMES = FALSE)
      if(sector == '')           value <- value
      if(sector %in% c('1','2') & pos %in% c('center'))                   value <- value + 6/2^(i+1)
      if(sector %in% c('1','2') & pos %in% c('bottomleft','bottomright')) value <- value + 6/2^(i)
      if(sector %in% c('3','4') & pos == 'center')                        value <- value - 6/2^(i+1)
      if(sector %in% c('3','4') & pos %in% c('topleft','topright'))       value <- value - 6/2^(i)
    }
    return(value)
  }
  #####
  for(g in 1:length(mtb)) {
    e[g] <- eastwest(s = subgrid[g], as.numeric(e[g]), pos = pos)
    n[g] <- northsouth(s = subgrid[g], value = as.numeric(n[g]), po = pos)
  }
  
  # decimal ####
  if(decimal) {
    ec <- floor(e/60)
    e <- ec + (e-ec*60)/60
    nc <- floor(n/60)
    n <- nc + (n-nc*60)/60
  }
  return(c(e,n))
}

