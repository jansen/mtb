# Coordinates will be transposed to Messtischblätter (d=1) or their
# subdivisions (MTBQ etc.)

eastnorth2mtb <- function(x,y, pos = 'center', d = 3, sep = '/') {
  if(pos != 'center') stop('Only center coordinates implemented.')
  if(any(is.na(x) | is.na(y))) stop('NA values in x or y detected. Please clean your input data.')
  if(any(x > 100 | y > 100)) stop('Coordinates must be given as decimal degrees.')
  e <- round(x,0)
  minute.x <- e*60 + (x - e)*60
  E <- floor((minute.x - 340)/10)
  x.rest <- (minute.x - 340)/10 - E    # subgrid minutes eastwest

  n <- y*60
  N <- ceiling((3354 - n)/6)
  y.rest <-  N - (3354 - n)/6          # subgrid minutes northeast
  R <- ''
  for(i in 1:d) {
    r <- vector('character', length = length(x))
    r[x.rest < 10/2^(i)/10 & y.rest >= 10/2^(i)/10] <- '1'
    r[x.rest >= 10/2^(i)/10 & y.rest >= 10/2^(i)/10] <- '2'
    r[x.rest < 10/2^(i)/10 & y.rest < 10/2^(i)/10] <- '3'
    r[x.rest >= 10/2^(i)/10 & y.rest < 10/2^(i)/10] <- '4'
    R <- paste(R,r, sep='')
    x.rest[x.rest >= 10/2^(i)/10] <- x.rest[x.rest >= 10/2^(i)/10] - 10/2^(i)/10
    y.rest[y.rest >= 10/2^(i)/10] <- y.rest[y.rest >= 10/2^(i)/10] - 10/2^(i)/10
  }
  return(paste(sprintf("%02d", N), sprintf("%02d", E), sep, R, sep=''))
}
